import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PokeApiService {
  baseUrl= "https://pokeapi.co/api/v2";

  constructor(private http: HttpClient) { }

  getAllPokemon(url: string){
    return this.http.get<any>(`${this.baseUrl+url}`);
  }

  getPokemonGeneration(url: string){
    return this.http.get<any>(`${this.baseUrl+url}`);
  }

  getPokemonVersionGroups(url: string){
    return this.http.get<any>(`${this.baseUrl+url}`);
  }

  getPokeApi(url: string){
    return this.http.get<any>(`${url}`);
  }
  
}
