import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PokeApiService } from 'src/app/services/poke-api.service';

@Component({
  selector: 'app-object-detail',
  templateUrl: './object-detail.component.html',
  styleUrls: ['./object-detail.component.scss']
})
export class ObjectDetailComponent implements OnInit {

  pokemonUrl:string = '/item/';
  pokedetail:string = '/pokemon/object/'  

  groupCategoria:any[]= [];
  item: any= '';  

  constructor(private pokeApi: PokeApiService, private router: Router, private activatedRouter: ActivatedRoute) { 
    this.activatedRouter.params.subscribe(
      params => {
        this.groupCategoria = [];
        this.getItemDetail(params['id']);
      }
    )
  }
  getItemDetail(id: any) {
    this.pokeApi.getAllPokemon(this.pokemonUrl+id).subscribe(
      res => {
        console.log(res);
       this.item = res;       
       this.getItemCategoria(res.category.url);
      },
      err => {
       console.log('problemas');
      }
     )    
  }
  getCategoriaItems(url: any) {
    let itemCategoria;
    this.pokeApi.getPokeApi(url).subscribe(
      res => {
        itemCategoria = {
          name: res.name,
          img: res.sprites.default,
        }

        this.groupCategoria.push(itemCategoria);
      },
      err => {
       console.log('problemas');
      }
     )
  }

  getItemCategoria(url:string){    
    let data: any[] = [];          
    this.pokeApi.getPokeApi(url).subscribe(
      res => {
        data = res.items;
        data.forEach(element => {
          this.getCategoriaItems(element.url)
        });
      },
      err => {
       console.log('problemas');
      }
     )
  }

  btnRedirect(id: string){
    this.router.navigateByUrl(this.pokedetail+id);
  }

  ngOnInit(): void {
  }

}
