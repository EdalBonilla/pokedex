import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { PokeApiService } from 'src/app/services/poke-api.service';

interface Order {
  value: string;
  viewValue: string;
};

@Component({
  selector: 'app-poke-all',
  templateUrl: './poke-all.component.html',
  styleUrls: ['./poke-all.component.scss']
})
export class PokeAllComponent implements OnInit {

  pokemonUrl:string = 'https://pokeapi.co/api/v2/pokemon?offset=0&limit=150';
  pokedetail:string = '/pokemon/detail/'

  displayedColumns =['id','name','img','type','btn'];   
  getParam:string = '';
  pokemon:any[]=[]
  dataSource = new MatTableDataSource<any>(this.pokemon);
  next:string;
  previous:string;
  control_button = true;

  order: Order[] = [
    {value: 'name_asc', viewValue: 'A-Z'},
    {value: 'name_desc', viewValue: 'Z-A'},        
    {value: 'default', viewValue: 'Defeact'}       
  ];
  selectedValue: string;

  constructor(private pokeApi: PokeApiService, private router: Router) { }

  ngOnInit(): void {
    this.getPokemonEntries(this.pokemonUrl);
  }

  getPokemonEntries(url:string){       
    let data: any[] = [];       
    this.pokeApi.getPokeApi(url).subscribe(
      res => {
        data = res.results;
        this.next = res.next;
        this.previous = res.previous;  
        data.forEach(element => {
          this.getPokemon(element.url)
        });
        
      },
      err => {
       console.log('problemas');
      }
     )     
  }

  getPokemon(url:string){    
    let pokemon_detail;    
    this.pokeApi.getPokeApi(url).subscribe(
      res => {        
        pokemon_detail = {
          id:res.id,          
          name:res.name,
          img:res.sprites.front_default,
          type:res.types[0].type.name,
        }

        this.pokemon.push(pokemon_detail);              
        this.pokemon.sort((a, b) => a.id - b.id);
        this.dataSource = new MatTableDataSource<any>(this.pokemon);

        
      },
      err => {
       console.log('problemas');
      }
     )
  }

  btnRedirect(id: string){
    this.router.navigateByUrl(this.pokedetail+id);
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  onFilter(){

    switch(this.selectedValue) { 
      case 'name_desc': { 
        this.pokemon.sort(function (a, b) {
          return b.name.localeCompare(a.name);
        });
        this.dataSource = new MatTableDataSource<any>(this.pokemon);
         break; 
      } 
      case 'name_asc': { 
        this.pokemon.sort(function (a, b) {
          return a.name.localeCompare(b.name);
        });
        this.dataSource = new MatTableDataSource<any>(this.pokemon);
        break; 
      } 
      case 'number_pokedex': { 
        this.pokemon.sort((a, b) => a.n_poke - b.n_poke);
        this.dataSource = new MatTableDataSource<any>(this.pokemon);
        break; 
      }
       
      default: { 
        this.pokemon.sort((a, b) => a.id - b.id);
        this.dataSource = new MatTableDataSource<any>(this.pokemon);
         break; 
      } 
   } 

  }

  btnRefresTable(control:string){

    if(control == 'next'){
      this.getPokemonEntries(this.next);      
    }else{
      this.getPokemonEntries(this.previous);
    }

  }

}
