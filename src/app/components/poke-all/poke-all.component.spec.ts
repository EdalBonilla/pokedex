import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokeAllComponent } from './poke-all.component';

describe('PokeAllComponent', () => {
  let component: PokeAllComponent;
  let fixture: ComponentFixture<PokeAllComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokeAllComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokeAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
