import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PokeApiService } from 'src/app/services/poke-api.service';
import {ThemePalette} from '@angular/material/core';
import {ProgressBarMode} from '@angular/material/progress-bar';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent implements OnInit {

  pokemon: any= '';
  pokemon_specie: any= '';
  pokemonImg = '';
  pokemonType = [];
  evolution: any[] = [];
  moves: any[] = [];
  displayedColumns =['name','learning']; 
  dataSource = new MatTableDataSource<any>(this.moves);

  getParam:string = '';
  pokemonUrl:string = '/pokemon/';
  pokemonSpeciesUrl:string = '/pokemon-species/';
  pokedetail:string = '/pokemon/detail/'

  pokemonDetail:any= {};
  
  color: ThemePalette = 'warn';
  mode: ProgressBarMode = 'determinate';
  value = 50;
  bufferValue = 75;

  constructor(private pokeApi: PokeApiService, private router: Router, private activatedRouter: ActivatedRoute) { 
    this.activatedRouter.params.subscribe(
      params => {
        this.evolution = [];
        this.getPokemonDetail(params['id']);
      }
    )
  }

  ngOnInit(): void {
    console.log(this.evolution);
  }

  getPokemonDetail(pokemonName:number){
    let datos: any[] =[];
    let pokemon_detail; 
    this.pokeApi.getAllPokemon(this.pokemonUrl+pokemonName).subscribe(
      res => {
       this.pokemon = res;
       this.pokemonImg = this.pokemon.sprites.front_default;
       this.pokemonType = res.types[0].type.name;
       datos = this.pokemon.moves;

       datos.forEach(element => {
        pokemon_detail = {
           name:element.move.name,
           learning: element.version_group_details[0].level_learned_at           
         };
         this.moves.push(pokemon_detail);
         this.dataSource = new MatTableDataSource<any>(this.moves);
         
       });

       



       this.getPokemonSpecie(res.name);
      },
      err => {
       console.log('problemas');
      }
     )    
  }

  getPokemonSpecie(pokemonName:string){          
    this.pokeApi.getAllPokemon(this.pokemonSpeciesUrl+pokemonName).subscribe(
      res => {
        this.pokemon_specie = res;
        this.getPokemonEvolution(res.evolution_chain.url);
      },
      err => {
       console.log('problemas');
      }
     )
  }

  getPokemonEvolution(url:string){          
    this.pokeApi.getPokeApi(url).subscribe(
      res => {
        if(res.chain.evolves_to != null){
          if(res.chain.species != null){
            this.getpokemonChain(res.chain.species.name);            
          }         
          if(res.chain.evolves_to[0].species != null){
            this.getpokemonChain(res.chain.evolves_to[0].species.name);
          }
          if(res.chain.evolves_to[0].evolves_to[0].species != null){
            this.getpokemonChain(res.chain.evolves_to[0].evolves_to[0].species.name);
          }
        }
      },
      err => {
       console.log('problemas');
      }
     )
  }

  getpokemonChain(pokemonName:string){
    let pokemonEvolution;
    this.pokeApi.getAllPokemon(this.pokemonUrl+pokemonName).subscribe(
      res => {
        pokemonEvolution = {
          name: res.name,
          img: res.sprites.front_default,
        }

        this.evolution.push(pokemonEvolution);
      },
      err => {
       console.log('problemas');
      }
     )
  }

  btnRedirect(id: string){
    this.router.navigateByUrl(this.pokedetail+id);
  }

}
