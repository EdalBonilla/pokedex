import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { PokeApiService } from 'src/app/services/poke-api.service';

interface Order {
  value: string;
  viewValue: string;
};

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})

export class PokemonListComponent implements OnInit {
  versionGroupUrl:string = '/version-group/';
  generationUrl:string = '/generation';
  pokedexUrl:string = '/pokedex/';
  pokemonSpeciesUrl:string = '/pokemon-species/';
  pokemonUrl:string = '/pokemon/';
  pokedetail:string = '/pokemon/detail/'

  displayedColumns =['id','name','img','type','btn'];   
  getParam:string = '';
  pokemon:any[]=[]
  dataSource = new MatTableDataSource<any>(this.pokemon);

  order: Order[] = [
    {value: 'name_asc', viewValue: 'A-Z'},
    {value: 'name_desc', viewValue: 'Z-A'},    
    {value: 'number_pokedex', viewValue: '# pokedex'},
    {value: 'default', viewValue: 'Defeact'}       
  ];
  selectedValue: string;

  constructor(private pokeApi: PokeApiService, private router: Router, private activatedRouter: ActivatedRoute) { 
    this.activatedRouter.params.subscribe(
      params => {
        this.getParam = params['id'];
      }
    )
  }

  ngOnInit(): void {
    this.getGroupVersions(this.getParam);
  }

  getGroupVersions(nameVersionGroups: string){
    //Metodo Obtiene la version del pokedex segun su version
    let data: any[] = [];
    let title: any;
    this.pokeApi.getPokemonVersionGroups(this.versionGroupUrl+nameVersionGroups).subscribe(
      res => {
          
          data = res.pokedexes;
          this.getPokemonsEntries(data[0].name)       
                 

      },
      err => {
        console.log('problemas');
      }
    )
  }

  getPokemonsEntries(pokedex:string){
    //Metodo obtiene los pokemons ingresados en el pokedex
    let data: any[] = [];
    this.pokeApi.getAllPokemon(this.pokedexUrl+pokedex).subscribe(
      res => {       
        data = res.pokemon_entries; //entry_number

        data.forEach(element => {          
          this.getPokemonEntries(element.pokemon_species.name, element.entry_number);
        });          

             
      },
      err => {
        console.log('problemas');
      }
    )
  }

  getPokemonEntries(pokemonName:number, n_pokedex:number){          
    this.pokeApi.getAllPokemon(this.pokemonSpeciesUrl+pokemonName).subscribe(
      res => {
        this.getPokemon(res.id,n_pokedex);
      },
      err => {
       console.log('problemas');
      }
     )
  }

  getPokemon(pokemonId:number, n_pokedex:number){    
    let pokemon_detail;
    
    this.pokeApi.getAllPokemon(this.pokemonUrl+pokemonId).subscribe(
      res => {
        pokemon_detail = {
          id:res.id,
          n_poke: n_pokedex,
          name:res.name,
          img:res.sprites.front_default,
          type:res.types[0].type.name,
        }

        this.pokemon.push(pokemon_detail);              
        this.pokemon.sort((a, b) => a.id - b.id);
        this.dataSource = new MatTableDataSource<any>(this.pokemon);

        
      },
      err => {
       console.log('problemas');
      }
     )
  }

  btnRedirect(id: string){
    this.router.navigateByUrl(this.pokedetail+id);
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  onFilter(){

    switch(this.selectedValue) { 
      case 'name_desc': { 
        this.pokemon.sort(function (a, b) {
          return b.name.localeCompare(a.name);
        });
        this.dataSource = new MatTableDataSource<any>(this.pokemon);
         break; 
      } 
      case 'name_asc': { 
        this.pokemon.sort(function (a, b) {
          return a.name.localeCompare(b.name);
        });
        this.dataSource = new MatTableDataSource<any>(this.pokemon);
        break; 
      } 
      case 'number_pokedex': { 
        this.pokemon.sort((a, b) => a.n_poke - b.n_poke);
        this.dataSource = new MatTableDataSource<any>(this.pokemon);
        break; 
      }
       
      default: { 
        this.pokemon.sort((a, b) => a.id - b.id);
        this.dataSource = new MatTableDataSource<any>(this.pokemon);
         break; 
      } 
   } 

  }

}
