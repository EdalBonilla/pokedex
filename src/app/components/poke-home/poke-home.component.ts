import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokeApiService } from 'src/app/services/poke-api.service';



@Component({
  selector: 'app-poke-home',
  templateUrl: './poke-home.component.html',
  styleUrls: ['./poke-home.component.scss']
})
export class PokeHomeComponent implements OnInit {
  generationUrl:string = '/generation';
  versionGroupUrl:string = '/version/';
  tiles: any[] = [];
   
  constructor(private pokeApi: PokeApiService, private router: Router) { }

  ngOnInit(): void {
    this.getGenerationPokemon();
  }

  getGenerationPokemon(){
    let data: any[] = [];
    let title: any;

    this.pokeApi.getPokemonGeneration(this.generationUrl).subscribe(
      res => {        
          data = res.results;         

          data.forEach(element => {
            title = {
              text: element.name, 
              cols: 1, 
              rows: 2, 
              color: 'lightblue'
   
            }
            this.tiles.push(title);
          });          

      },
      err => {
        console.log('problemas');
      }
    )
  }

  btnRedirect(generation_name: string){
    this.router.navigateByUrl(this.versionGroupUrl+generation_name);
  }

}
