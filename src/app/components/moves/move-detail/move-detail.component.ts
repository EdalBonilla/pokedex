import { Component, OnInit } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { PokeApiService } from 'src/app/services/poke-api.service';

@Component({
  selector: 'app-move-detail',
  templateUrl: './move-detail.component.html',
  styleUrls: ['./move-detail.component.scss']
})
export class MoveDetailComponent implements OnInit {

  pokemonUrl:string = '/move/';
  pokedetail:string = '/pokemon/item/'  

  groupCategoria:any[]= [];
  item: any= '';  
  powerfull: any= '';  

  color: ThemePalette = 'warn';
  mode: ProgressBarMode = 'determinate';
  value = 50;
  bufferValue = 75;

  constructor(private pokeApi: PokeApiService, private router: Router, private activatedRouter: ActivatedRoute) { 
    this.activatedRouter.params.subscribe(
      params => {
        this.groupCategoria = [];
        this.getItemDetail(params['id']);
      }
    )
  }
  getItemDetail(id: any) {
    this.pokeApi.getAllPokemon(this.pokemonUrl+id).subscribe(
      res => {
       this.item = res;
       this.powerfull = [
         {name:"PP", value:this.item.pp},
         {name:"Power", value:this.item.power},
         {name:"Accuracy", value:this.item.accuracy},
       ]             
      },
      err => {
       console.log('problemas');
      }
     )    
  }
  btnRedirect(id: string){
    this.router.navigateByUrl(this.pokedetail+id);
  }

  ngOnInit(): void {
  }

}
