import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMovesComponent } from './list-moves.component';

describe('ListMovesComponent', () => {
  let component: ListMovesComponent;
  let fixture: ComponentFixture<ListMovesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListMovesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMovesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
