import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokeVersionComponent } from './poke-version.component';

describe('PokeVersionComponent', () => {
  let component: PokeVersionComponent;
  let fixture: ComponentFixture<PokeVersionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokeVersionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokeVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
