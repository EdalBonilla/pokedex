import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PokeApiService } from 'src/app/services/poke-api.service';

@Component({
  selector: 'app-poke-version',
  templateUrl: './poke-version.component.html',
  styleUrls: ['./poke-version.component.scss']
})
export class PokeVersionComponent implements OnInit {

  versionGroupUrl:string = '/version-group/';
  generationUrl:string = '/generation';
  
  getParam:string = '';
  tiles: any[] = [];

  constructor(private pokeApi: PokeApiService, private router: Router, private activatedRouter: ActivatedRoute) {
    this.activatedRouter.params.subscribe(
      params => {
        this.getParam = params['name'];
      }
    )
   }

  ngOnInit(): void {
    this.getVersion(this.getParam);
  }

  getVersion(nameGeneration: string){
    let data: any[] = [];
    let title: any;

    this.pokeApi.getAllPokemon(this.generationUrl+'/'+nameGeneration).subscribe(
      res => {          
          data = res.version_groups;         
          data.forEach(element => {         
            this.getGroupVersions(element.name);            
          });

         

      },
      err => {
        console.log('problemas');
      }
    )
  }

  getGroupVersions(nameVersionGroups: string){
    let data: any[] = [];
    let title: any;

    this.pokeApi.getPokemonVersionGroups(this.versionGroupUrl+nameVersionGroups).subscribe(
      res => {
          
          data = res.versions;        

          data.forEach(element => {
            console.log(element.name); 
            title = {
              text: element.name, 
              cols: 1, 
              rows: 2, 
              color: 'lightblue',
              param: nameVersionGroups
   
            }
            this.tiles.push(title); 
          });

          

      },
      err => {
        console.log('problemas');
      }
    )
  }

  btnRedirect(version_groups: string){
    this.router.navigateByUrl('/pokedex'+'/'+version_groups);
  }
  

}
