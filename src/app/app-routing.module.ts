import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListMovesComponent } from './components/moves/list-moves/list-moves.component';
import { MoveDetailComponent } from './components/moves/move-detail/move-detail.component';
import { ListObjectsComponent } from './components/objects/list-objects/list-objects.component';
import { ObjectDetailComponent } from './components/objects/object-detail/object-detail.component';
import { PokeAllComponent } from './components/poke-all/poke-all.component';
import { PokeHomeComponent } from './components/poke-home/poke-home.component';
import { PokeVersionComponent } from './components/poke-version/poke-version.component';
import { PokemonDetailComponent } from './components/pokemon-detail/pokemon-detail.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';

const routes: Routes = [
  {path: 'home', component: PokeHomeComponent },
  {path: 'version/:name', component: PokeVersionComponent },
  {path: 'pokemon/national', component: PokeAllComponent },
  {path: 'pokedex/:id', component: PokemonListComponent },
  {path: 'pokemon/detail/:id', component: PokemonDetailComponent },
  {path: 'pokemon/objects', component: ListObjectsComponent },
  {path: 'pokemon/object/:id', component: ObjectDetailComponent },
  {path: 'pokemon/moves', component: ListMovesComponent },
  {path: 'pokemon/move/:id', component: MoveDetailComponent },
  {path: '', pathMatch: 'full', redirectTo: 'home' },
  {path: '**', pathMatch: 'full', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
