import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { PokemonDetailComponent } from './components/pokemon-detail/pokemon-detail.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from 'src/shared/material.module';
import { HttpClientModule } from '@angular/common/http';
import { PokeHomeComponent } from './components/poke-home/poke-home.component';
import { PokeAllComponent } from './components/poke-all/poke-all.component';
import { PokeVersionComponent } from './components/poke-version/poke-version.component';
import { ListObjectsComponent } from './components/objects/list-objects/list-objects.component';
import { ObjectDetailComponent } from './components/objects/object-detail/object-detail.component';
import { ListMovesComponent } from './components/moves/list-moves/list-moves.component';
import { MoveDetailComponent } from './components/moves/move-detail/move-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    PokemonDetailComponent,
    PokemonListComponent,
    PokeHomeComponent,
    PokeAllComponent,
    PokeVersionComponent,
    ListObjectsComponent,
    ObjectDetailComponent,
    ListMovesComponent,
    MoveDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
